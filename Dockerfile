FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app

RUN mkdir -p /HRMS.Services.Identity/ 
RUN mkdir -p /HRMS.Services.Identity.Data/
RUN mkdir -p /HRMS.Services.Identity.Core/
RUN mkdir -p /HRMS.Services.Identity.Items/

COPY ./src/HRMS.Services.Identity/. ./HRMS.Services.Identity
COPY ./src/HRMS.Services.Identity.Data/. ./HRMS.Services.Identity.Data
COPY ./src/HRMS.Services.Identity.Core/. ./HRMS.Services.Identity.Core
COPY ./src/HRMS.Services.Identity.Items/. ./HRMS.Services.Identity.Items

COPY ./nuget.config ./src/HRMS.Services.Identity/

RUN dotnet restore ./HRMS.Services.Identity/HRMS.Services.Identity.csproj

RUN dotnet publish ./HRMS.Services.Identity/ -c Release -o /app/out

FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime

WORKDIR /app

COPY --from=build /app/out .

ENV ASPNETCORE_URLS http://*:5000
ENV ASPNETCORE_ENVIRONMENT Docker

EXPOSE 5000

ENTRYPOINT ["dotnet","HRMS.Services.Identity.dll"]