﻿using AutoMapper;
using Consul;
using HRMS.Common;
using HRMS.Common.Consul;
using HRMS.Common.Dispatchers;
using HRMS.Common.Handlers;
using HRMS.Common.I18N;
using HRMS.Common.Mvc;
using HRMS.Common.Swagger;
using HRMS.Common.Validators;
using HRMS.Services.Identity.Core;
using HRMS.Services.Identity.Data;
using HRMS.Services.Identity.Items.Entities;
using HRMS.Services.Identity.Items.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Reflection;
using System.Text;

namespace HRMS.Services.Identity
{
    public class Startup
    {
        private static readonly string[] exposedHeaders = new[] { "X-Total-Count" };

        private readonly ILogger<Startup> logger;

        public Startup(ILogger<Startup> logger, IConfiguration configuration)
        {
            this.logger = logger;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddI18N();

            services.AddCustomMvc(opt => ConfigureOptions(opt))
                    .AddAuthorization(opt => ConfigurePolicies(opt))
                    .AddValidators(typeof(Startup));

            ConfigureIoc(services);

            ConfigureAuthentication(services);

            services.AddConsul();

            services.AddSwaggerDocs();

            services.AddLogging(x => x.AddConsole());

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", cors =>
                        cors.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials()
                            .WithExposedHeaders(exposedHeaders));
            });

            services.AddAutoMapper(Assembly.GetAssembly(typeof(Startup)));

            services.AddDispatchers();

            services.AddRepositories(typeof(CoreIdentifierType));

            services.AddHandlers(typeof(Startup));
        }

        private void ConfigureIoc(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), x =>
            {
                x.MigrationsAssembly(typeof(DataIdentifierType).Namespace);
                x.MigrationsHistoryTable("ef_migrations_history", "public");
            }));

            services.AddIdentity<AppUser, IdentityRole>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequiredLength = 6;
            })
              .AddEntityFrameworkStores<ApplicationContext>();

            services.AddScoped(typeof(DbContext), typeof(ApplicationContext));
        }

        private void ConfigureOptions(MvcOptions opt)
        {
            var policy = new AuthorizationPolicyBuilder()
                             .RequireAuthenticatedUser()
                             .Build();

            opt.Filters.Add(new AuthorizeFilter(policy));
        }

        private void ConfigurePolicies(AuthorizationOptions opt)
        {
        }

        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              IApplicationLifetime applicationLifetime,
                              IConsulClient client)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "Docker")
            {
                app.UseDeveloperExceptionPage();

                if (env.EnvironmentName == "Docker")
                {
                    InitializeDatabase(app);
                }
            }

            var consulServiceId = app.UseConsul();

            applicationLifetime.ApplicationStopped.Register(() =>
            {
                client.Agent.ServiceDeregister(consulServiceId);
            });

            app.UseAuthentication();

            app.UseCors("CorsPolicy");

            app.UseErrorHandler();

            app.UseI18N();

            app.UseSwaggerDocs();

            app.UseMvc();
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            IConfiguration configuration;
            using (var serviceProvider = services.BuildServiceProvider())
            {
                configuration = serviceProvider.GetService<IConfiguration>();
            }

            var section = configuration.GetSection("jwt");
            var options = configuration.GetOptions<JwtOptions>("jwt");

            services.Configure<JwtOptions>(section);

            services
                .AddAuthentication(cfg =>
                {
                    cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    cfg.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = options.Issuer,
                        ValidAudience = options.Issuer,
                        ValidateLifetime = options.ValidateLifetime,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.SecretKey)),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            logger.LogInformation("Database migrating...");

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var provider = serviceScope.ServiceProvider;

                var db = provider.GetService<ApplicationContext>();

                db.Database.Migrate();
            }

            logger.LogInformation("Database migrated successfully.");
        }
    }
}
