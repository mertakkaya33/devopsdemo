﻿using HRMS.Common.Exceptions;
using HRMS.Common.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading;

namespace HRMS.Services.Identity.Controllers
{
    [Route("api/identity/v1/service")]
    public class ServiceController : ServiceBaseController
    {
        private readonly IHostingEnvironment environment;
        private readonly IServiceId serviceId;
        private readonly IOptions<AppOptions> appOptions;

        public ServiceController(IHostingEnvironment hostingEnvironment, IServiceId service, IOptions<AppOptions> applicationOptions)
        {
            environment = hostingEnvironment ?? throw new DependencyInjectionArgumentNullException(nameof(hostingEnvironment));
            serviceId = service ?? throw new DependencyInjectionArgumentNullException(nameof(service));
            appOptions = applicationOptions ?? throw new DependencyInjectionArgumentNullException(nameof(applicationOptions));
        }

        [HttpGet]
        public IActionResult ServiceInformation()
        {
            List<string> headers = new List<string>();

            foreach (var key in HttpContext.Request.Headers.Keys)
            {
                headers.Add($"{key} : {HttpContext.Request.Headers[key]}");
            }

            var info = new
            {
                serviceName = "Identity service",
                id = serviceId.Id,
                appId = appOptions.Value.Id,
                appName = appOptions.Value.Name,
                environment.EnvironmentName,
                culture = Thread.CurrentThread.CurrentCulture,
                uiculture = Thread.CurrentThread.CurrentUICulture,
                headers
            };

            return Ok(info);
        }

    }
}
