﻿using HRMS.Services.Identity.Items.Entities;
using HRMS.Services.Identity.Items.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.Services.Identity.Controllers
{
    [Route("api/identity/v1/accounts")]
    [ApiController]
    [AllowAnonymous]
    public class IdentityController : ServiceBaseController
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly IOptions<JwtOptions> options;

        public IdentityController(UserManager<AppUser> userManager,
                                  IOptions<JwtOptions> jwtOptions,
                                  SignInManager<AppUser> userSignInManager)
        {
            this.userManager = userManager;

            options = jwtOptions;

            signInManager = userSignInManager;
        }

        [HttpGet("me")]
        public IActionResult Get()
        {
            return Ok();
        }

        [HttpPatch("me/password")]
        public IActionResult ChangePassword()
        {
            return Ok();
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignIn()
        {
            var result = await signInManager.PasswordSignInAsync("abdulkadir.sen@neyasis.com", "abdulkadir", false, false);

            if (result.Succeeded == false)
            {
                return BadRequest();
            }

            var appUser = userManager.Users.FirstOrDefault(r => r.Email == "abdulkadir.sen@neyasis.com");

            string token = GenerateJwtToken("abdulkadir.sen@neyasis.com", appUser);

            var tokenizer = new
            {
                Token = token
            };

            return Ok(tokenizer);
        }

        [HttpPost("")]
        public async Task<IActionResult> SignUp()
        {
            var user = new AppUser
            {
                UserName = "abdulkadir.sen@neyasis.com",
                Email = "abdulkadir.sen@neyasis.com",
            };

            var result = await userManager.CreateAsync(user, "abdulkadir");

            if (result.Succeeded == false)
            {
                return BadRequest(result);
            }

            await signInManager.SignInAsync(user, false);

            string token = GenerateJwtToken(user.Email, user);

            var tokenizer = new
            {
                Token = token
            };


            return Ok(tokenizer);
        }

        [HttpPost("me/proxy")]
        public IActionResult GetProxy()
        {
            return Ok();
        }

        private string GenerateJwtToken(string email, IdentityUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.Value.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddSeconds(Convert.ToDouble(options.Value.ExpirySeconds));

            var token = new JwtSecurityToken(
                options.Value.Issuer,
                options.Value.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}