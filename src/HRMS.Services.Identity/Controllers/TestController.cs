﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HRMS.Services.Identity.Controllers
{
    [Route("api/identity/v1/test")]
    public class TestController : ServiceBaseController
    {
        [HttpGet("")]
        public IActionResult Test()
        {
            return Ok("yee");
        }
    }
}