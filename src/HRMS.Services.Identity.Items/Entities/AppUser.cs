﻿using HRMS.Common.Types;
using Microsoft.AspNetCore.Identity;

namespace HRMS.Services.Identity.Items.Entities
{
    public class AppUser : IdentityUser
    {
        public string RegistryNumber { get; set; }
    }
}
