﻿using HRMS.Services.Identity.Items.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HRMS.Services.Identity.Data
{
    public class ApplicationContext : IdentityDbContext<AppUser>
    {
        private readonly string schema = "identity";

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(schema);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityRole>(entity => { entity.ToTable(name: "app_roles"); });
            modelBuilder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("app_user_roles"); });
            modelBuilder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("app_user_claims"); });
            modelBuilder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("app_user_logins"); });
            modelBuilder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("app_user_tokens"); });
            modelBuilder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("app_role_claims"); });


            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataIdentifierType).Assembly);
        }
    }
}
