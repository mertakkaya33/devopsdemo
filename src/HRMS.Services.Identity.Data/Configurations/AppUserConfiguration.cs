﻿using HRMS.Services.Identity.Items.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HRMS.Services.Identity.Data.Configurations
{
    public class AppUserConfiguration : IdentityUserConfiguration<AppUser>
    {
        public override void Configure(EntityTypeBuilder<AppUser> builder)
        {
            base.Configure(builder);

            builder.Property(x => x.RegistryNumber)
                   .HasMaxLength(40)
                   .HasColumnName("registry_number")
                   .HasColumnType("varchar");
            //.IsRequired();
        }
    }
}
