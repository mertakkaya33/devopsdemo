﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HRMS.Services.Identity.Data.Configurations
{
    public class IdentityUserConfiguration<TUser> : IEntityTypeConfiguration<TUser> where TUser : IdentityUser
    {
        public virtual void Configure(EntityTypeBuilder<TUser> builder)
        {
            builder.ToTable("app_users");
        }
    }
}
