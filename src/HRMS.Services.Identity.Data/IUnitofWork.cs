﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace HRMS.Services.Identity.Data
{
    public interface IUnitofWork : IDisposable
    {
        DbSet<T> AsQueryable<T>() where T : class;

        Task<int> SaveChangesAsync();
    }
}
