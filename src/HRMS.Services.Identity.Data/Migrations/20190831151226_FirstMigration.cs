﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMS.Services.Identity.Data.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "identity");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
